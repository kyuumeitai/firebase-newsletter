const fs = require('fs')
const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const createCsvWriter = require('csv-writer').createArrayCsvWriter

const app = express()

const port = process.env.PORT || 3000
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

const newusers = require('./newsletter-users.json');

const newsletterdb = require('./newsletters.json')
const userkeys = Object.keys(newsletterdb.users)

const findUser = (localid) => {
  return newusers.users.find((user) => {
    return user.localId === localid
  })
}

const findUserByEmail = (email) => {
  return newusers.users.find((user) => {
    return user.email === email
  })
}

const getNews = () => {
  const allnewslist = []
  for (const key of userkeys) {
    const newsletters = newsletterdb.users[key].newsletters
    const usernewsletters = Object.keys(newsletters)
    allnewslist.push(...usernewsletters)
  }
  const news = new Set(allnewslist)
  return [...news]
}

const news = getNews()

news.map((newsletter) => {
  const newsletterobj = new Object()
  newsletterobj.newsletter = newsletter
  newsletterobj.emails = []

  for(const key of userkeys) {
    const newsletters = newsletterdb.users[key].newsletters
    const usernewsletters = Object.keys(newsletters)
    const user = findUser(key)

    const isInNewsletter = usernewsletters.find((usernewsletters) => {
      return usernewsletters === newsletter
    })
    if (isInNewsletter){
      newsletterobj.emails.push([user.email])
      // console.log(`Sí está en ${newsletter}: ${user.email}`)
    } else {
      // console.log(`No está en ${newsletter}: ${user.email}`)
    }
  }
  console.log(newsletterobj.emails)

  const csvWriter = createCsvWriter({
    path: `${newsletter}.csv`,
    header: ['Email']
  })
  csvWriter.writeRecords(newsletterobj.emails)
    .then(() => {
      console.log(`...Escrito ${newsletter}.csv con ${newsletterobj.emails.length} registros.`)
    })
})

app.listen(port)
console.log('badabim, badabam, badabuuuum:', port)